module Skyhub
  class HellTriangle
    attr_reader :triangle

    def initialize at = []
      @triangle = []

      at.each do |row|
        self.add(row)
      end
    end

    def add row
      validate_size_row(row)
      validate_itens_row(row)
      @triangle << row
      self
    end

    def sum
      return 0 if @triangle.size == 0

      max = 0
      
      @triangle.last.each_with_index do |value, index|
        current = HellTriangle.sum_maximum_three(@triangle, @triangle.size - 1, index)
        max = current > max ? current : max
      end

      max
    end

    private
      def validate_itens_row row        
        row.each do |n|
          raise ArgumentError.new("Invalid type of elements.") unless n.class == Fixnum
        end
      end

      def validate_size_row row
        if ( @triangle.empty? && row.size > 1 ) || ( @triangle.any? && row.size - @triangle.last.size > 1 )
          size = @triangle.any? ? @triangle.last.size + 1 : 1 
          raise ArgumentError.new("Invalid size of row. Expect row size #{size}")
        end
      end

      def self.sum_maximum_three arr, line, index
        if index < 0 || index >= arr[line].size
          return 0
        elsif line == 0
          return arr[0][0]
        else
          current = arr[line][index]
          left    = current + HellTriangle.sum_maximum_three(arr, line - 1, index - 1)
          right   = current + HellTriangle.sum_maximum_three(arr, line - 1, index)
          return left > right ? left : right
        end
      end
  end
end