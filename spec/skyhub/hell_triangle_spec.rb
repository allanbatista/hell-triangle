require 'skyhub'

describe Skyhub::HellTriangle do
  subject { Skyhub::HellTriangle }

  context ".new" do
    it "Must initialize empty triangle" do
      hell = subject.new

      expect( hell.triangle ).to be_empty
    end

    it "Must initialize with full triangle" do
      hell = nil

      expect {
        hell = subject.new([[6],[3,5],[9,7,1],[4,6,8,4]])
      }.not_to raise_error

      expect(hell.triangle.size).to eq(4)
      expect(hell.sum).to eq(26)
    end

    it "Must initialize with full triangle with a complex tree" do
      hell = nil

      expect {
        hell = subject.new([[1],[1,10],[1,1,10],[1,1,1,10],[1,1,1,1,10],[1,100,1,1,1,10]])
      }.not_to raise_error

      expect(hell.triangle.size).to eq(6)
      expect(hell.sum).to eq(114)
    end

    it "Must not initialize with full triangle with wrong element type" do
      hell = nil

      expect {
        hell = subject.new([[6],[3,5],[9,'a',1],[4,6,8,4]])
      }.to raise_error(/Invalid type of elements./)

      expect(hell).to be_nil
    end

    it "Must not initialize with full triangle with wrong size of row" do
      hell = nil

      expect {
        hell = subject.new([[6],[9,7,1],[3,5],[4,6,8,4]])
      }.to raise_error(/Invalid size of row\. Expect row size 2/)

      expect(hell).to be_nil
    end
  end

  context "#add" do
    it "Must add array as row: Top to Bottom" do
      hell = subject.new

      hell.add( [1] )
      expect( hell.triangle ).to contain_exactly([1])
      
      hell.add( [2, 3] )
      expect( hell.triangle ).to contain_exactly([1], [2, 3])
    end

    it "Must add only array interges" do
      hell = subject.new

      expect {
        hell.add( ['a'] )
      }.to raise_error(/Invalid type of elements./)

      expect {
        hell.add( [1.9] )
      }.to raise_error(/Invalid type of elements./)
    end

    it "Must add first row with size greater than 1" do
      hell = subject.new
      
      expect {
        hell.add( [1, 2] )
      }.to raise_error(/Invalid size of row\. Expect row size 1/)
    end

    it "Must add only when size of array is +1 than current bottom" do
      hell = subject.new
      
      hell.add( [3] )

      expect {
        hell.add( [3, 4, 5] )
      }.to raise_error(/Invalid size of row\. Expect row size 2/)
    end
  end

  context "#sum" do
    it "Must return 0 with a empty triangle" do
      hell = subject.new

      summed = hell.sum

      expect(summed).to eq(0)
    end

    it "Must return 6 with triangle with one level" do
      hell = subject.new

      hell.add([6])

      summed = hell.sum

      expect(summed).to eq(6)
    end

    it "Must return 11 with triangle with trow level" do
      hell = subject.new

      hell.add([6]).add([3,5])

      summed = hell.sum

      expect(summed).to eq(11)
    end

    it "Must return 18 with triangle with trow level" do
      hell = subject.new

      hell.add([6]).add([3,5]).add([9,7,1])

      summed = hell.sum

      expect(summed).to eq(18)
    end

    it "Must return a 26 with sum a triangle" do
      hell = subject.new

      hell.add([6]).add([3,5]).add([9,7,1]).add([4,6,8,4])

      summed = hell.sum

      expect(summed).to eq(26)
    end

    it "Must summed left tree" do
      hell = subject.new

      hell.add([6]).add([5, 3]).add([1,9,7]).add([4,6,8,4]).add([15,1,1,1,1])

      summed = hell.sum

      expect(summed).to eq(31)
    end

    it "Must summed right tree" do
      hell = subject.new

      hell.add([6]).add([5, 3]).add([1,9,7]).add([4,6,8,4]).add([1,1,1,1,15])

      summed = hell.sum

      expect(summed).to eq(35)
    end

    it "Must summed center tree" do
      hell = subject.new

      hell.add([6]).add([5, 3]).add([1,9,7]).add([4,6,8,4]).add([1,1,15,1,1])

      summed = hell.sum

      expect(summed).to eq(43)
    end
  end
end